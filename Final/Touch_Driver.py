"""
@file Touch_Driver.py

@brief The Touch_Driver class measures the X,Y,Z locations of a touch on the resistive touch screen
@details The Touch_Driver class takes in 4 Pin objects as well as the active length and width of the screen
The constructor assigns these values as well as creates conversion constants for the output.
the three individual methods scan the X, Y and Z locations of the screen where the latter represents
a Bool. of weather a touch is occurring. the X and Y components are outputted as measurements deviating
from the center of the screen in meters. The final Method scans all three axis and returns a tuple of
the data. each method is optimized for speed

@author Matt Murray
"""

from pyb import Pin
import pyb

class Touch_Driver:
    '''
    @brief A class to scan the X Y and Z positions on a screen
    @details Uses built in functionality of the ADC and the Pin classes from micropython to read the location of a
    press on the resistive touch screen. locations are determined along each axis by setting up a resistive bridge
    network similar to a plus sign where one pin is set high and another low. this creates a resistor divider
    that allows us to determine the location of the press by the number of ADC counts read on the ADC.
    readings are given in meters for all methods.
    '''

    def __init__(self, P1, P2, P3, P4, L, W):
        """
        @brief Constructor assigns pin objects as well as calculates scaling parameter for output.
        @param P1 Pin object assigned to X drain
        @param P2 Pin object assigned to X source
        @param P3 Pin object assigned to Y drain
        @param P4 Pin object assigned to Y source
        @param L Length(X) of the active section of the resistive touch screen
        @param W Width(Y) if the active section of the resistive touch screen
        """
        ## pin assignment for X drain
        self.P_xm = P1
        ## pin assignment for X source
        self.P_xp = P2
        ## pin assignment for Y drain
        self.P_ym = P3
        ## pin assignment for Y source
        self.P_yp = P4
        ## length assignment
        self.L = L
        ## width assignment
        self.W = W

        ## X axis conversion for ADC -> meters
        self.X_conv = L/3560
        ## Y axis conversion for ADC -> meters
        self.Y_conv = W/3220

    def Xscan(self):
        """
        @brief Method to determine position of touch in X axis
        @details This method energizes the Xp(source) pin and sets the Xm(drain) as ground. Then by reading from
        the Ym(drain) pin we are able to find the ADC reading at that position. By scaling the reading we are able to
        achieve an output in m deviated from the center
        @return returns the distance in (m) that the touch has deviated from center
        """
        self.P_xm.init(mode=Pin.OUT_PP, value=0)
        self.P_xp.init(mode=Pin.OUT_PP, value=1)
        self.P_ym.init(mode=Pin.IN)
        self.P_yp.init(mode=Pin.IN)

        self.P_ym.init(mode=Pin.ANALOG)
        ADC_ym = pyb.ADC(self.P_ym)
        return(self.X_conv*(2020-ADC_ym.read()))


    def Yscan(self):
        """
        @brief Method to determine position of touch in Y axis
        @details This method energizes the Yp(source) pin and sets the Mm(drain) as ground. Then by reading from
        the Xm(drain) pin we are able to find the ADC reading at that position. By scaling the reading we are able to
        achieve an output in m deviated from the center
        @return returns the distance in (m) that the touch has deviated from center
        """
        self.P_xm.init(mode=Pin.IN)
        self.P_xp.init(mode=Pin.IN)
        self.P_ym.init(mode=Pin.OUT_PP, value=0)
        self.P_yp.init(mode=Pin.OUT_PP, value=1)

        self.P_xm.init(mode=Pin.ANALOG)
        ADC_xm = pyb.ADC(self.P_xm)
        return(self.Y_conv*(1958-ADC_xm.read()))

    def Zscan(self):
        """
        @brief Method to determine is a press is made (Z axis)
        @details This method energizes the Yp(source) pin and sets the Xm(drain) as ground. Then by reading from
        the Ym(drain) pin we are able to see if the line has been pulled up or down. with no press the pin is pulled
        up to roughly the max ADC count. Then when pressed it is pulled down to about half of the ADC count.
        @return returns a boolean is True if a press and False if not
        """
        self.P_xm.init(mode=Pin.OUT_PP, value=0)
        self.P_xp.init(mode=Pin.IN)
        self.P_ym.init(mode=Pin.IN)
        self.P_yp.init(mode=Pin.OUT_PP, value=1)

        self.P_ym.init(mode=Pin.ANALOG)
        ADC_ym = pyb.ADC(self.P_ym)
        return((ADC_ym.read() < 4000))

    def Allscan(self):
        """
        @brief Method to determine position of touch in X, Y and Z axies
        @details By using the software components of the three methods: Xscan, Yscan and Zscan, we are able to
        find all the location data for a touch. By performing the measurements in the order of X,Z,Y we are able
        to reduce the execution time. This is done by limiting the number of pin assignments
        @return returns a tuple of measurements in the order of (X,Y,Z) with units of (m,m,Bool.)
        """

        #read X
        self.P_xm.init(mode=Pin.OUT_PP, value=0)
        self.P_xp.init(mode=Pin.OUT_PP, value=1)
        self.P_ym.init(mode=Pin.IN)
        self.P_yp.init(mode=Pin.IN)

        udelay(4)

        self.P_ym.init(mode=Pin.ANALOG)
        ADC_ym = pyb.ADC(self.P_ym)
        X = self.X_conv*(2020-ADC_ym.read())

        #read Z
        self.P_xp.init(mode=Pin.IN)
        self.P_yp.init(mode=Pin.OUT_PP, value=1)
        udelay(4)
        Z = (ADC_ym.read() < 4000)

        #read Y
        self.P_xm.init(mode=Pin.IN)
        self.P_xp.init(mode=Pin.IN)
        self.P_ym.init(mode=Pin.OUT_PP, value=0)
        self.P_yp.init(mode=Pin.OUT_PP, value=1)

        self.P_xm.init(mode=Pin.ANALOG)
        udelay(4)
        ADC_xm = pyb.ADC(self.P_xm)

        return((X, self.Y_conv*(1958-ADC_xm.read()), Z))