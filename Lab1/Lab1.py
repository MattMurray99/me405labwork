""" @file       Lab1.py
    @brief      this file executes the vendotron functionality
    @details    this function is run via the cmd line and uses the keys:
                [0=pennies,1=nickels,2=dimes,3=quarters,4=ones,5=fives,6=tens,7=twenties]
                to input money into the program. Then the user can use:
                [p=popsi, s=spryte, c=cuke, d=Dr.Popper, e=eject]
                to select drink choice and or eject change
    @author     Matt Murray
    @date       1/20/2021
    @image html FSM_diagram.PNG
    @image latex FSM_diagram.PNG "FSM Diagram for Lab1" width=30cm
"""

import keyboard

def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name

def DivNSubtract(value, divisor):
    ##
    # @brief      Helper Function for GetChange()
    # @details    function floor divides by the divisor to determine the
    #            number of denominations in the value and outputs the
    #            remaining value with denominations removed
    # @param      value   float value of money
    # @param      divisor float value used to floor divide

    result = value // divisor
    if result >= 1:
        output = value-(result*divisor)
        return [output, result]
    else:
        output = value
        return output, result

def getChange(price, payment):
    ##
    # @brief      Determine change given for any given payment/price
    # @details    This function takes in a payment amount and subtracts the price.
    #            it then checks for insufficient funds. If sufficient then DivNSubtract()
    #            is invoked iteratively to give change in the least number of denominations
    # @param  price   float value of a price
    # @param  payment float value of current funds
    # @return     Returns a tuple containing amount of change given after a putchase. the format
    #            of this tuple is [20's, 10's, 5's, 1's, .25, .10, .05, .01]

    rawChange = payment-price
    changeValues = [20, 10, 5, 1, .25, .10, .05, .01]
    OutputChange = []
    #iterate through denominations
    for d in changeValues:
        rawChange, num = DivNSubtract(rawChange, d)
        OutputChange.append(int(num))

    print("Change Format: [20's, 10's, 1's, Quarters, Dimes, Nickels, Pennies]")
    print(OutputChange)
    return OutputChange

def printWelcome():
    ##
    # @brief      Prints welcome string to terminal
    # @details    Function prints multiple lines of text to instruct the user
    #            on how to operate the program


    print("-----------------------------------------" '\n'
          "           Welcome to Vendotron          " '\n'
          " Keys: 0=Pennies  1=Nickels 2=Dimes      " '\n'
          "       3=Quarters 4=1s      5=5s         " '\n'
          "       6=10s      7=20s                  " '\n'
          "  c=Cuke  p=Popsi  s=Spryte  d=Dr.Pupper " '\n'
          "               e=Eject                   " '\n'
          "-----------------------------------------" '\n'
          " Prices: Cuke = $2.25   Popsi = $2.50    " '\n'
          "         Spryte = $2.00 Dr.Pupper = 4.50 " '\n'
          "-----------------------------------------" '\n'
          " Note: Please press firmly on btns       ")

#initialization parameters

##  @brief      State variable
#   @details    Variable to control the state of the program
state = 0

##  @brief      Balance of the user
#   @details    This variable controls the current balance of the user
balance = 0

last_key = ''

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("c", callback=kb_cb)
keyboard.on_release_key("p", callback=kb_cb)
keyboard.on_release_key("s", callback=kb_cb)
keyboard.on_release_key("d", callback=kb_cb)
keyboard.on_release_key("e", callback=kb_cb)
keyboard.on_release_key("0", callback=kb_cb)
keyboard.on_release_key("1", callback=kb_cb)
keyboard.on_release_key("2", callback=kb_cb)
keyboard.on_release_key("3", callback=kb_cb)
keyboard.on_release_key("4", callback=kb_cb)
keyboard.on_release_key("5", callback=kb_cb)
keyboard.on_release_key("6", callback=kb_cb)
keyboard.on_release_key("7", callback=kb_cb)

# Run this loop forever, or at least until someone presses control-C
while True:
    if last_key == 'c':
        last_key = 'c'
    elif last_key == 'p':
        last_key = 'p'
    elif last_key == 's':
        last_key = 's'
    elif last_key == 'd':
        last_key = 'd'
    elif last_key == 'e':
        last_key = 'e'
    elif last_key == '0':
        last_key = '0'
    elif last_key == '1':
        last_key = '1'
    elif last_key == '2':
        last_key = '2'
    elif last_key == '3':
        last_key = '3'
    elif last_key == '4':
        last_key = '4'
    elif last_key == '5':
        last_key = '5'
    elif last_key == '6':
        last_key = '6'
    elif last_key == '7':
        last_key = '7'

    # below is the code for the FSM

    if state == 0:      #Initialization step
        printWelcome()
        balance = 0
        state = 1

    elif state == 1:    #Home screen
        #print("Please enter drink selection or insert money")
        if last_key in "01234567":
            #indicates a coin is pressed, move from home to balance update
            state = 2
        elif last_key in "cpsd":
            #drink is selected, move to balance check
            state = 3
        elif last_key == "e":
            #eject btn pressed, moving to eject coins
            state = 6
        else:
            print("Key not valid, please press valid key")

    elif state == 2:    #Balance update
        if last_key == '0':
            balance += .01
            print("Pennie inserted. Balance = $",balance)
        elif last_key == '1':
            balance += .05
            print("Nickel inserted. Balance = $",round(balance,2))
        elif last_key == '2':
            balance += .1
            print("Dime inserted. Balance = $", round(balance,2))
        elif last_key == '3':
            balance += .25
            print("Quarter inserted. Balance = $",round(balance,2))
        elif last_key == '4':
            balance += 1
            print("One dollar inserted. Balance = $",round(balance,2))
        elif last_key == '5':
            balance += 5
            print("Five dollars inserted. Balance = $",round(balance,2))
        elif last_key == '6':
            balance += 10
            print("Ten dollars inserted. Balance = $",round(balance,2))
        elif last_key == '7':
            balance += 20
            print("Twenty dollars inserted. Balance = $",round(balance,2))
        balance = round(balance, 2)
        last_key = ''
        state = 1

    elif state == 3:    #Balance Check
        if (last_key == 'c') & (balance >= 2.25):
            state = 5
        elif (last_key == 'p') & (balance >= 2.50):
            state = 5
        elif (last_key == 's') & (balance >= 2.00):
            state = 5
        elif (last_key == 'd') & (balance >= 4.50):
            state = 5
        else:
            #move to insufficient funds
            state = 4

    elif state == 4:    #Low Funds Page
        print("insufficient funds")
        last_key = ''
        state = 1

    elif state == 5:    #Vend selected
        if last_key == 'c':
            print("Cuke Dispensed, subtracting from balance ...")
            balance -= 2.25
            print("balance remaining: $",balance)
        elif last_key == 'p':
            print("Popsi Dispensed, subtracting from balance ...")
            balance -= 2.50
            print("balance remaining: $",balance)
        elif last_key == 's':
            print("Spryte Dispensed, subtracting from balance ...")
            balance -= 2.00
            print("balance remaining: $",balance)
        elif last_key == 'd':
            print("Dr.Popper Dispensed, subtracting from balance ...")
            balance -= 4.50
            print("balance remaining: $",balance)
        else:
            print("Key not valid, please press valid key")
        last_key = ''
        state = 1

    elif state == 6:    #Eject change
        change = getChange(0,balance)
        balance = 0
        last_key = ''
        state = 0




