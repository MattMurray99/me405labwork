'''
@file Lab3_UI.py
@brief This file is a UI that reads user input and processes data from the Nucleo board
@details This file is ran on the users computer and prompts the user to press a G to start the board.
        after the user presses a G character the board collects the data on the external interrupt and sends to this program via
        serial port. The program then creates a .CSV file containing raw adc values and their respective timestamps
        then after this the program scales the raw data to voltage and plots the data.
@author     Matt Murray
@date       2/4/2021
'''

import serial
import numpy as np
import matplotlib.pyplot as plt
import csv



#set print to supress scientific notation
np.set_printoptions(suppress=True)


ser = serial.Serial(port='COM6', baudrate=115273, timeout=1)

def sendChar():

    ##
    # @brief Function to send and read a character form the board
    # @details This function pols the user to press a key. the key is transmitted to the board and the board sends it
    # back to confirm the press. the echo is read and returned

    inv = input('Press G to begin:')
    print("Button pressed:", str(inv))
    ser.write(str(inv).encode('ascii'))

    returnkey = ser.readline().decode('ascii')
    print(returnkey)
    return returnkey

##  @brief      An Array of raw ADC values
#   @details    this array contains raw ADC values and is populated from the serial port
raw_values = np.array([])

##  @brief      An Array of scaled ADC values
#   @details    This array contains scaled(0,3.3) voltage values
scaled_values = np.array([])

##  @brief      An array of timestamps
#   @details    This array contains timestamps ranging from .001 to .2
timestamp_values = np.array([])

##  @brief      number of lines to be read
#   @details    counter to control the number of reads from the serial port
num_lines = 0

##  @brief      User input key
#   @details    this returnkey is the echo from the user input from the board
returnkey = 'S'

while returnkey != 'G':
    #wait for correct key to begin
    returnkey = sendChar()



while(num_lines < 199):
    if ser.in_waiting != 0:
        #read an entry from COM6
        value = ser.readline()

        #convert to float and strip '/n' characters
        floatvalue = float(value.rstrip())

        #append values to output array
        raw_values = np.append(raw_values, floatvalue)

        num_lines += 1

#####################################
# create CSV file
#####################################
timestamp_values = np.around(np.linspace(.001, .200, 199), decimals=3)

with open('rawDataOutput.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["Seconds", "ADC value"])
    i = 0
    while i<len(timestamp_values):
        writer.writerow([timestamp_values[i], raw_values[i]])
        i = i+1

#####################################
#plot data using matplotlib
#####################################

#scale data to Voltage and round to 3 decimal places
scaled_values = np.around((3.3/4095)*raw_values, decimals=3)
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.plot(timestamp_values, scaled_values, color='tab:orange')
ax.set_ylim([0,3.5])
ax.set_ylabel('Voltage /V')
ax.set_xlim([0,.2])
ax.set_xlabel('Time /s')
ax.set_title('ADC Reading of Button Press')
plt.show()


