"""
@file main.py
@brief This program measures the step response of a button press.
@details This file is ran on the microcontroller and is triggered by the user pressing a G on the keyboard
when prompted. The program then waits for an external interrupt on the button and then measures 200
ADC samples at 1000Hz. These samples are then converted to strings delimited by '\n' and written
to the serial port.
@author Matt Murray
@date 2/4/2021
"""

import pyb
from pyb import UART
import array

#light object
blinko = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)

#ADC object
adc = pyb.ADC(pyb.Pin.board.PA0)

#buffer object
buffer = array.array('H', (0 for index in range (200)))

#UART serial port instance
uart = UART(2)

def readADC_isr(what_timer):
    ##
    # @brief      ISR for recording ADC values
    # @details    function is invoked when the user button is pressed. 200 samples are then recorded
    #             and the ADC flag is set to 1
    # @param      what_timer  dummy variable for interpeter


    adc.read_timed(buffer, 1000)
    blinko.low()
    global adc_flag
    adc_flag = 1

## UART object for communicating with board
extint = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP,readADC_isr)

pyb.ExtInt.disable(extint)

##  @brief      ADC flag value
#   @details    flag value used to signal when the isr has been ran and buffer is populated
adc_flag = 0

##  @brief      Start Key
#   @details    char to be read from serial port to start the program
key = 'S'

##  @brief      augmented buffer
#   @details    buffer that holds augmented data ready to be transmitted
aug_buffer = 0

while key != 'G':
    #check if there are bits in the buffer
    if uart.any() != 0:
        key = chr(uart.readchar())
        uart.write(str(key))

pyb.ExtInt.enable(extint)
blinko.high()
while(True):
    if adc_flag == 1:
        adc_flag = 0
        #augment buffer
        aug_buffer = str(buffer).replace(', ', '\n')
        aug_buffer = aug_buffer.replace("array('H'\n[", '')

        uart.write(aug_buffer)
