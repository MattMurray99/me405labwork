"""
@file       main.py
@brief      This file is one that tests and runs the Touch_Driver class for the resistive touchscreen

@details    In this file we assign pin values to micropython pins and create an instance of the
            Touch_Driver class with these pins as well as the active touch area for the screen.
            the while loop runs roughly every one sec and calls one method of the class and measures
            execution time.

@author     Matt Murray
@date       3/3/2021
"""
import pyb
from pyb import Pin
import utime
from Touch_Driver import Touch_Driver


# pin assignments:
# ym = PA0
# xm = PA1
# yp = PA6
# xp = PA7

##  @brief      Pin object for Touch_Driver
#   @details    Pin represents the X drain of the resistor brdge
P_xm = Pin.board.PA1

##  @brief      Pin object for Touch_Driver
#   @details    Pin represents the X source of the resistor brdge
P_xp = Pin.board.PA7

##  @brief      Pin object for Touch_Driver
#   @details    Pin represents the Y drain of the resistor brdge
P_ym = Pin.board.PA0

##  @brief      Pin object for Touch_Driver
#   @details    Pin represents the Y Source of the resistor brdge
P_yp = Pin.board.PA6

##  @brief      Length of screen (X)
#   @details    active region of the screen measured in (m)
L = .176

##  @brief      Width of screen (Y)
#   @details    active region of the screen measured in (m)
W = .103

##  @brief      Touch_Driver opbject
#   @details    Object created by calling class Touch_Driver with the parameters given above
td = Touch_Driver(P_xm, P_xp, P_ym, P_yp, L, W)
utime.sleep(2)


while True:
    utime.sleep(1)
    start_time = utime.ticks_us()
    reading = td.Allscan()
    end_time = utime.ticks_us()
    total_time = utime.ticks_diff(end_time, start_time)
    print(str(reading) + ' {:}'.format(total_time))


