import cv2
import numpy as np
import math
import pandas


strfile = 'foothill_swerve_1.mp4'
#strfile = 'Driving1.MOV'

def drawWarningLeft(frame, color):
    x = 600
    y = 700
    if color == 'yellow':
        color_code = (0, 255, 255)
    if color == 'red':
        color_code = (0, 0, 255)

    cv2.line(frame, (x, y-15), (x, y+15), color_code, 2)
    cv2.line(frame,  (x, y+30), (x, y+35), color_code, 4)
    cv2.circle(frame, (x,y+10), 35, color_code, 2)

def drawWarningRight(frame, color):
    x = 1400
    y = 700
    if color == 'yellow':
        color_code = (0, 255, 255)
    if color == 'red':
        color_code = (0, 0,255)

    cv2.line(frame, (x, y-15), (x, y+15), color_code, 2)
    cv2.line(frame,  (x, y+30), (x, y+35), color_code, 4)
    cv2.circle(frame, (x,y+10), 35, color_code, 2)

def getFirstFrame(videofile):
    vidcap = cv2.VideoCapture(videofile)
    success, image = vidcap.read()
    if success:
        cv2.imwrite("first_frame.jpg", image)

cap = cv2.VideoCapture(strfile)
def show_slide(filename, current_frame):
    slide = cv2.imread(filename)
    if current_frame == None:
        re_slide = cv2.resize(slide, (192, 108), interpolation=cv2.INTER_AREA)
        cv2.imshow('Frame', re_slide)
        cv2.waitKey()
        return
    else:
        re_slide = cv2.resize(slide, (1920, 1080), interpolation=cv2.INTER_AREA)
        cv2.addWeighted(re_slide, 0.6, current_frame, 0.4, 0, current_frame)
        cv2.imshow('Frame', frame)
        cv2.waitKey(0)
        return


speed = 1
success = 0
failure = 0
font = cv2.FONT_HERSHEY_SIMPLEX
prev_left_mean_line = None
prev_right_mean_line = None
frame_cnt = 0
show_slide('intro_slide.png', None)
show_slide('what.png', None)
show_slide('goal.png', None)
show_slide('why.png', None)
show_slide('measure.png', None)
show_slide('how.png', None)

while(True):

    ret, frame = cap.read()
    frame_cnt += 1
    print("Starting Demo")
    if frame is not None:

        stencil = np.zeros_like(frame[:, :, 0])
        polygon = np.array([[450, 750], [800, 500], [1100, 500], [1400, 750]])
        stencil = cv2.fillConvexPoly(stencil, polygon, 1)

        #stencil_frame = cv2.addWeighted(stencil, 0.2, frame, 0.8, 0)
        if frame_cnt == 1:
            cv2.imshow('Frame', frame)
            cv2.waitKey(0)
        if frame_cnt <50 and frame_cnt > 1:
            cv2.imshow('Frame', frame)
            if cv2.waitKey(speed) & 0xFF == ord('q'):
                break
        if frame_cnt == 50:
            cv2.waitKey(0)

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        if frame_cnt > 50 and frame_cnt <100:
            cv2.imshow('Frame', gray)
            if cv2.waitKey(speed) & 0xFF == ord('q'):
                break


        ret1, thresh1 = cv2.threshold(gray, 160, 180, cv2.THRESH_BINARY)
        img = cv2.bitwise_and(frame[:, :, 0], frame[:, :, 0], mask=stencil)
        if frame_cnt == 100:
            cv2.waitKey(0)
            cv2.imshow('Frame', thresh1)
            cv2.waitKey(0)
            edges = cv2.Canny(thresh1, 300, 500)
            cv2.imshow('Frame', edges)
            cv2.waitKey(0)
            lines = cv2.HoughLinesP(thresh1, 1, np.pi / 180, 30,
                                    maxLineGap=200, minLineLength=200)
            HoughLinesDrawing = np.zeros_like(frame)
            if lines is not None:
                for line in lines:
                    x1, y1, x2, y2 = line[0]
                    angle = math.degrees(math.atan2(x2 - x1, y2 - y1))
                    # if angle < 60:
                    cv2.line(HoughLinesDrawing, (x1, y1), (x2, y2), (255, 255, 255))

                    # if angle > 120:
                    #    cv2.line(HoughLinesDrawing, (x1,y1), (x2,y2), (255,255,255))

                    # else:
                    #    pass

            cv2.imshow('Frame', HoughLinesDrawing)
            cv2.waitKey(0)
            frame_example = frame.copy()
            if lines is not None:
                for line in lines:
                    x1, y1, x2, y2 = line[0]
                    angle = math.degrees(math.atan2(x2 - x1, y2 - y1))
                    if (angle < 50 and angle > 40) or (angle > 130 and angle < 140):
                        cv2.line(HoughLinesDrawing, (x1, y1), (x2, y2), (0, 255, 0), 4)
                        cv2.line(frame_example, (x1, y1), (x2, y2), (0, 255, 0), 4)

            cv2.imshow('Frame', HoughLinesDrawing)
            cv2.waitKey(0)
            cv2.imshow('Frame', frame_example)
            cv2.waitKey(0)
            cv2.imshow('Frame', img)
            cv2.waitKey(0)
            ret, thresh = cv2.threshold(img, 160, 180, cv2.THRESH_BINARY)
            cv2.imshow('Frame', thresh)




        #img = cv2.bitwise_and(frame[:, :, 0], frame[:, :, 0], mask=stencil)


        ret, thresh = cv2.threshold(img, 160, 180, cv2.THRESH_BINARY)
        lines = cv2.HoughLinesP(thresh, 1, np.pi / 180, 30,
                                maxLineGap=200, minLineLength=200)

        edges = cv2.Canny(thresh, 300, 500)

        if frame_cnt == 101:
            cv2.imshow('Frame', thresh)
            cv2.waitKey(0)
            cv2.imshow('Frame', edges)
            HoughLinesDrawing = np.zeros_like(thresh)
            if lines is not None:
                for line in lines:
                    x1, y1, x2, y2 = line[0]
                    angle = math.degrees(math.atan2(x2 - x1, y2 - y1))
                    if angle < 60:
                        cv2.line(HoughLinesDrawing, (x1,y1), (x2,y2), (255,255,255))

                    if angle > 120:
                        cv2.line(HoughLinesDrawing, (x1,y1), (x2,y2), (255,255,255))

                    else:
                        pass

            cv2.imshow('Frame', HoughLinesDrawing)
            cv2.waitKey(0)
        if frame_cnt == 102:
            cv2.waitKey(0)

        ### Rev 1
        rev1 = frame.copy()
        if frame_cnt > 102 and frame_cnt < 200:
            blur = cv2.GaussianBlur(img, (5, 5), 1)
            edges = cv2.Canny(blur, 50, 150, edges=None, apertureSize=None, L2gradient=None)
            minLineLength = 5000
            maxLineGap = 25
            lines = cv2.HoughLinesP(edges, 1, np.pi / 180, 100, minLineLength, maxLineGap)

            list_left = []
            list_right = []
            for line in lines:
                x1, y1, x2, y2 = line[0]
                angle = math.degrees(math.atan2(x2 - x1, y2 - y1))
                dist = int(math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2))
                # limit to angles we will see
                if (angle < 150):
                    # remove stencil lines
                    if (int(angle) != 119) & (int(angle) != 118) & (int(angle) != 90) & (int(angle) != 62) & (
                            int(angle) != 63):
                        # add angles to L/R lists
                        if angle > 90:
                            list_left.append((x1, y1, x2, y2, angle, dist))
                        elif angle <= 90:
                            list_right.append((x1, y1, x2, y2, angle, dist))

            # creaing dataframes from line data
            d = {'x1': [], 'y1': [], 'x2': [], 'y2': [], 'ang': [], 'dist': []}
            DF_L = pandas.DataFrame(data=d)
            DF_R = pandas.DataFrame(data=d)
            for d in list_left:
                DF_L = DF_L.append({'x1': d[0], 'y1': d[1], 'x2': d[2], 'y2': d[3], 'ang': d[4], 'dist': d[5]},
                                   ignore_index=True)
            for d in list_right:
                DF_R = DF_R.append({'x1': d[0], 'y1': d[1], 'x2': d[2], 'y2': d[3], 'ang': d[4], 'dist': d[5]},
                                   ignore_index=True)

            # remove NaN values
            DF_L.dropna()
            DF_R.dropna()

            # finding the largest distance line found and drawing it

            if DF_L.size != 0:
                maxidx_L = DF_L.dist.idxmax()
                cv2.line(frame, (int(DF_L.x1[maxidx_L]), int(DF_L.y1[maxidx_L])),
                         (int(DF_L.x2[maxidx_L]), int(DF_L.y2[maxidx_L])), (0, 0, 255), 3)

                angle_L = DF_L.ang[maxidx_L]
                if (angle_L >= 138) & (angle_L < 146):
                    drawWarningLeft(frame, 'yellow')
                elif angle_L >= 146:
                    drawWarningLeft(frame, 'red')

            # drawWarningRight(frame)
            if DF_R.size != 0:
                maxidx_R = DF_R.dist.idxmax()
                cv2.line(frame, (int(DF_R.x1[maxidx_R]), int(DF_R.y1[maxidx_R])),
                         (int(DF_R.x2[maxidx_R]), int(DF_R.y2[maxidx_R])), (0, 255, 0), 3)

                angle_R = DF_R.ang[maxidx_R]
                if (angle_R <= 35) & (angle_R > 30):
                    drawWarningRight(frame, 'yellow')
                elif angle_R <= 30:
                    drawWarningRight(frame, 'red')

            cv2.imshow("Frame", frame)

            if cv2.waitKey(speed) & 0xFF == ord('q'):
                break
        if frame_cnt == 200:
            cv2.waitKey(0)



        ### Rev 2
        dmy = frame.copy()

        list_left = []
        list_right = []
        if lines is not None:
            for line in lines:
                x1, y1, x2, y2 = line[0]
                angle = math.degrees(math.atan2(x2 - x1, y2 - y1))
                if angle < 60:
                    list_left.append((x1,y1,x2,y2,angle))

                if angle > 120:
                    list_right.append((x1,y1,x2,y2,angle))

                else:
                    pass


        d = {'x1': [], 'y1': [], 'x2': [], 'y2': [], 'ang': []}

        DF_L = pandas.DataFrame(data=d)
        DF_R = pandas.DataFrame(data=d)

        for d in list_left:
            DF_L = DF_L.append({'x1': d[0], 'y1': d[1], 'x2': d[2], 'y2': d[3], 'ang': d[4]},
                               ignore_index=True)

        for d in list_right:
            DF_R = DF_R.append({'x1': d[0], 'y1': d[1], 'x2': d[2], 'y2': d[3], 'ang': d[4]},
                               ignore_index=True)

        left_mean_line = DF_L.mean(skipna=True)
        right_mean_line = DF_R.mean(skipna=True)

        #try:
        #    optimal_line_p1 = (int((right_mean_line['x2'] + left_mean_line['x1'])/2),
        #                       int((right_mean_line['y2'] + left_mean_line['y1'])/2))
        #    optimal_line_p2 = (int((right_mean_line['x1'] + left_mean_line['x2'])/2),
        #                       int((right_mean_line['y1'] + left_mean_line['y2'])/2))

         #   cv2.line(dmy, optimal_line_p1, optimal_line_p2, (0, 0, 255), 10)
        #except:
         #   pass
        try:

            cv2.line(dmy, (int(left_mean_line['x1']), int(left_mean_line['y1'])),
                     (int(left_mean_line['x2']), int(left_mean_line['y2'])),
                     (0, 255, 0), 10)

            cv2.line(dmy, (int(right_mean_line['x1']), int(right_mean_line['y1'])),
                     (int(right_mean_line['x2']), int(right_mean_line['y2'])),
                     (0, 255, 0), 10)


        except:

            pass

        if frame_cnt >= 200 and frame_cnt < 300:
            cv2.imshow('Frame', dmy)
            if cv2.waitKey(speed) & 0xFF == ord('q'):
                break
        if frame_cnt == 300:
            cv2.waitKey(0)

        try:
            lane_mask = np.ones_like(dmy)
            lane_poly = np.array([[int(left_mean_line['x1']), int(left_mean_line['y1'])],
                                  [int(left_mean_line['x2']), int(left_mean_line['y2'])],
                                  [int(right_mean_line['x1']), int(right_mean_line['y1'])],
                                  [int(right_mean_line['x2']), int(right_mean_line['y2'])]])

            lane_block = cv2.fillConvexPoly(lane_mask, lane_poly, (0, 255, 0))
            cv2.addWeighted(lane_block, 0.2, dmy, 0.8, 0, dmy)
            success += 1
        except:
            failure += 1
            pass

        if frame_cnt > 300 and frame_cnt < 350:
            cv2.imshow('Frame', dmy)
            if cv2.waitKey(speed) & 0xFF == ord('q'):
                break

        try:
            if ((right_mean_line['ang'] >= 125) & (right_mean_line['ang'] < 130)):
                drawWarningRight(dmy, 'yellow')
            if right_mean_line['ang'] <= 125:
                drawWarningRight(dmy, 'red')
            if (left_mean_line['ang'] <= 55) & (left_mean_line['ang'] > 50):
                drawWarningLeft(dmy, 'yellow')
            if left_mean_line['ang'] >= 55:
                drawWarningLeft(dmy, 'red')
        except:
            pass

        if frame_cnt > 350:
            cap.release()




        #cv2.imshow('Frame', dmy)

        #if cv2.waitKey(speed) & 0xFF == ord('q'):
        #    break
    else:
        break

cv2.waitKey(0)
cap = cv2.VideoCapture(strfile)

stencil = np.zeros((1080, 1920))
polygon = np.array([[400, 750], [900, 500], [1100, 500], [1600, 750]])

stencil = cv2.fillConvexPoly(stencil, polygon, 1)

speed = 1
success = 0
failure = 0

prev_left_mean_line = None
prev_right_mean_line = None
while(True):

    ret, frame = cap.read()

    if frame is not None:

        stencil = np.zeros_like(frame[:, :, 0])
        polygon = np.array([[300, 750], [850, 500], [1100, 500], [1600, 750]])
        stencil = cv2.fillConvexPoly(stencil, polygon, 1)

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        img = cv2.bitwise_and(frame[:, :, 0], frame[:, :, 0], mask=stencil)

        ret, thresh = cv2.threshold(img, 160, 180, cv2.THRESH_BINARY)

        lines = cv2.HoughLinesP(thresh, 1, np.pi / 180, 30,
                                maxLineGap=200, minLineLength=200)

        dmy = frame.copy()

        list_left = []
        list_right = []
        if lines is not None:
            for line in lines:
                x1, y1, x2, y2 = line[0]
                angle = math.degrees(math.atan2(x2 - x1, y2 - y1))
                if angle < 60:
                    list_left.append((x1,y1,x2,y2,angle))

                if angle > 120:
                    list_right.append((x1,y1,x2,y2,angle))

                else:
                    pass

        d = {'x1': [], 'y1': [], 'x2': [], 'y2': [], 'ang': []}

        DF_L = pandas.DataFrame(data=d)
        DF_R = pandas.DataFrame(data=d)

        for d in list_left:
            DF_L = DF_L.append({'x1': d[0], 'y1': d[1], 'x2': d[2], 'y2': d[3], 'ang': d[4]},
                               ignore_index=True)

        for d in list_right:
            DF_R = DF_R.append({'x1': d[0], 'y1': d[1], 'x2': d[2], 'y2': d[3], 'ang': d[4]},
                               ignore_index=True)

        left_mean_line = DF_L.mean(skipna=True)
        right_mean_line = DF_R.mean(skipna=True)

        #try:
        #    optimal_line_p1 = (int((right_mean_line['x2'] + left_mean_line['x1'])/2),
        #                       int((right_mean_line['y2'] + left_mean_line['y1'])/2))
        #    optimal_line_p2 = (int((right_mean_line['x1'] + left_mean_line['x2'])/2),
        #                       int((right_mean_line['y1'] + left_mean_line['y2'])/2))

         #   cv2.line(dmy, optimal_line_p1, optimal_line_p2, (0, 0, 255), 10)
        #except:
         #   pass
        try:

            cv2.line(dmy, (int(left_mean_line['x1']), int(left_mean_line['y1'])),
                     (int(left_mean_line['x2']), int(left_mean_line['y2'])),
                     (0, 255, 0), 10)

            cv2.line(dmy, (int(right_mean_line['x1']), int(right_mean_line['y1'])),
                     (int(right_mean_line['x2']), int(right_mean_line['y2'])),
                     (0, 255, 0), 10)

            lane_mask = np.ones_like(dmy)
            lane_poly = np.array([[int(left_mean_line['x1']), int(left_mean_line['y1'])],
                                [int(left_mean_line['x2']), int(left_mean_line['y2'])],
                                [int(right_mean_line['x1']), int(right_mean_line['y1'])],
                                [int(right_mean_line['x2']), int(right_mean_line['y2'])]])

            lane_block = cv2.fillConvexPoly(lane_mask, lane_poly, (0,255,0))

            if ((right_mean_line['ang'] >= 125) & (right_mean_line['ang'] < 130)):
                drawWarningRight(dmy, 'yellow')
            if right_mean_line['ang'] <= 125:
                drawWarningRight(dmy, 'red')
            if (left_mean_line['ang'] <= 55) & (left_mean_line['ang'] > 50):
                drawWarningLeft(dmy, 'yellow')
            if left_mean_line['ang'] >= 55:
                drawWarningLeft(dmy, 'red')

            cv2.addWeighted(lane_block, 0.2, dmy, 0.8, 0, dmy)
            success += 1

        except:
            failure += 1
            pass

        cv2.imshow('Frame', dmy)

        if cv2.waitKey(speed) & 0xFF == ord('q'):
            break
    else:
        break


cap.release()
cv2.waitKey(0)
cv2.destroyAllWindows()
print(success)
print(failure)
print(success/(success+failure))
