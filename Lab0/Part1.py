import sys
import cv2
import numpy as np

###################################
#Part 1
###################################


print('Number of arguments:', len(sys.argv), 'arguments')
print('Argument List:', str(sys.argv))

movFile = 'red.mov'

while True:
    try:
        print('File Selected from CMD line:', str(sys.argv[1]))
        cap = cv2.VideoCapture(sys.argv[1])
        break
    except IndexError:
        print('File Selected from Script:', movFile)
        cap = cv2.VideoCapture(movFile)
        break

imarr = []                                # create an array for all images

while(cap.isOpened()):
    ret, frame = cap.read()

    if not ret:
        break

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)   # convert frame to grey
    blur = cv2.GaussianBlur(gray, (9, 9), 2)         # add blur to the grey frame

    rows = blur.shape[0]
    circles = cv2.HoughCircles(blur, cv2.HOUGH_GRADIENT, 1, rows/8,
                               param1=100, param2=50, minRadius=1,
                               maxRadius=100)

    if circles is not None:
        circles = np.uint16(np.around(circles))      #convert to uint16
        for i in circles[0,:]:
            center = (i[0], i[1])  # outline circle center
            radius = i[2]          # outline circle radius
            cv2.circle(frame, center, radius, (0, 255, 0), 2)       #draw circle on color image


    cv2.imshow('frame', frame)                        # show color image

    if cv2.waitKey(30) & 0xFF == ord('q'):
        break
cap.release()                                        # release capture

cv2.waitKey(0)
cv2.destroyAllWindows()
