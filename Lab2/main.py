"""
@file       main.py
@brief      This file executes the reaction time program on the ST micro.
@details    This function is ran on the microcontroller after the user pushes the program
            to the device and presses the reset button. the program uses the user button as an int.
            the program will flash an LED and the user will press the btn to test reaction time.
            when the user presses CTRL-C the program ends and reports average reaction time.
@author     Matt Murray
@date       1/23/2021
"""


import pyb
import utime
import urandom

## contains a timer object for use in reaction timing
tim = pyb.Timer(2, prescaler=79, period=1000000)

## contains a pin object used to blink the LED
blinko = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)

def count_isr(what_timer):
    ##
    # @brief      ISR for recording reaction time
    # @details    function is invoked when the user button is pressed. the time is recorded
    #             and the number of tries is increased
    # @param      what_timer  dummy variable for interpeter

    global count
    global tries
    count = tim.counter()
    tries += 1


extint = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP,count_isr)


#variables for calculating avg

##  @brief      reaction time value
#   @details    each reaction time value is added to this running sum
reaction = 0

##  @brief      number of tries done
#   @details    increased every time the btn is pressed
tries = 0

##  @brief      Timer count result
#   @details    variable contains the number of uS of reaction time
count = 0

##  @brief      Random delay variable
#   @details    Variable controls the number of sec(2s-3s) to delay before the next test
delay = 0
pyb.ExtInt.enable(extint)

try:
    while(True):
        pyb.ExtInt.disable(extint)                    #turn off interupt
        delay = urandom.randint(2000, 3000)     #pick a random time to delay
        utime.sleep_ms(delay)                   #delay
        blinko.high()                           #turn pin on
        pyb.ExtInt.enable(extint)                     #turn on interupt
        tim.counter(0)                          #set counter to 0
        utime.sleep_ms(1000)                    #wait to turn off (ISR happens here)
        blinko.low()                            #turn led off
        reaction += count


except KeyboardInterrupt:
    if (tries == 0):
        print("No Reaction Time Calculates! Try again")
    else:
        print("Average Reaction Time = ", ((reaction / tries) / 1000000), 's')


