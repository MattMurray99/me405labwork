"""
@file       main.py
@brief

@details

@author     Matt Murray
@date       3/3/2021
"""
import pyb
from pyb import Pin
import utime
from Encoder import Encoder
from MotorDriver import MotorDriver



btn_pin = pyb.Pin(pyb.Pin.board.PC13, pyb.Pin.IN, pyb.Pin.PULL_UP)
fault_pin = pyb.Pin(pyb.Pin.board.PB2, pyb.Pin.IN, pyb.Pin.PULL_UP)

#sleep_pin = pyb.Pin(pyb.Pin.board.PA15, pyb.Pin.OUT_PP)

def setFault_isr(what_btn):
    ## do somethin
    print('Fault Entered')
    global FaultFlag
    FaultFlag = 1
    motoX.set_duty(0)
    motoY.set_duty(0)
    while FaultFlag == 1:
        pass

def clearFault_isr(what_btn):
    ## do something
    global FaultFlag
    FaultFlag = 0
    print('Fault Cleared')


extint2 = pyb.ExtInt(fault_pin, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, setFault_isr)
extint1 = pyb.ExtInt(btn_pin, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, clearFault_isr)

pyb.ExtInt.enable(extint1)
pyb.ExtInt.enable(extint2)

# pins for motor 1
IN1Y_pin = pyb.Pin.cpu.B4
IN2Y_pin = pyb.Pin.cpu.B5
#pins for motor 2
IN1X_pin = pyb.Pin.cpu.B0
IN2X_pin = pyb.Pin.cpu.B1
#sleep and timer for both motors
sleep_pin = pyb.Pin.cpu.A15
timer = 3


motoY = MotorDriver(sleep_pin, IN1Y_pin, IN2Y_pin, timer, 1, 2)
motoX = MotorDriver(sleep_pin, IN1X_pin, IN2X_pin, timer, 3, 4)


pyb.ExtInt.disable(extint2)
motoX.enable()
motoY.enable()
pyb.ExtInt.enable(extint2)













"""
# initialize encoder parameters
P1y = pyb.Pin.board.PB6          #encoder E1 channel 1
P2y = pyb.Pin.board.PB7          #encoder E1 channel 2
timery = 4

P1x = pyb.Pin.board.PC6          #encoder E2 channel 1
P2x = pyb.Pin.board.PC7         #encoder E2 channel 2
timerx = 8

encx = Encoder(P1x,P2x,timerx)
ency = Encoder(P1y,P2y,timery)

while True:
    #utime.sleep(2)
    encx.update()
    ency.update()
    posx = encx.getPosition()
    posy = ency.getPosition()
    print(posx,posy)
"""